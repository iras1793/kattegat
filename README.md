

![Kattegat, Text logo](assets/Kattegat-text.png)

The purpose of this project is to develop an administrator system for restaurants and bars where knowledge is applied 
in Django and React. 


___
## Development Tools

[![Python](https://img.shields.io/badge/Python-3475A8?style=for-the-badge&logo=python&logoColor=white)](https://www.python.org/) 
[![Javascript](https://img.shields.io/badge/JavaScript-E5A126?style=for-the-badge&logo=javascript&logoColor=white)](https://www.python.org/)
[![Django](https://img.shields.io/badge/Django-092D1F?style=for-the-badge&logo=django)](https://www.djangoproject.com/)
[![React](https://img.shields.io/badge/React-blue?style=for-the-badge&logo=react&logoColor=white)](https://es.reactjs.org/)
___
## Installation

To install follow the following checklist:
1. Download the repo
```bash
  $ git clone https://iras1793@bitbucket.org/iras1793/kattegat.git
```
2. Change to kattegat directory
```bash
  $ cd kattegat
```
3. Create the virtual environment
```bash
  $ python3 -m venv venv
```
4. Start the virtual environment
```bash
  $ . venv/bin/activate [Just for Linux and Mac users]
  or
  > venv\Scripts\activate [For Windows users]
```
5. Install the requirements
```bash
  (venv) pip install -r requirements.txt
```
6. Run the project
```bash
  (venv) python manage.py runserver 8000
```

___

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`SECRET_KEY`

___ 

## Author

- [Iras Dargor - Victor Cruz](http://irasdargor.com.mx)

___

## Support

For support, email **victorcruz1793@gmail.com** or follow me on:

[![Twitter](https://img.shields.io/badge/Twitter-1DABDD?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/Iras1793)
[![Linkedin](https://img.shields.io/badge/Linkedin-005B88?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/vcruz1793/)


___
## License
Copyright © Iras Dargor - Victor Cruz

Licensed under the [MIT license](https://choosealicense.com/licenses/mit/)

