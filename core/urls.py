from django.contrib import admin
from django.urls import path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Kattegat Docs",
      default_version='v1',
      description="Kattegat Documentation",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="victorcruz1793@gmail.com"),
      license=openapi.License(name="MIT License"),
   ),
   public=True,
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
