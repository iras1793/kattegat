import React from 'react'
import {Button} from "semantic-ui-react"

export default function App(){
  return (
      <div>
        <h1>Hola mundo!</h1>
        <Button primary>Dale!</Button>
      </div>
  )
}